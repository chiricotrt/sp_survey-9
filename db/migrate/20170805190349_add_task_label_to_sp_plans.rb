class AddTaskLabelToSpPlans < ActiveRecord::Migration
  def change
    add_column :sp_plans, :task_label, :string
  end
end
