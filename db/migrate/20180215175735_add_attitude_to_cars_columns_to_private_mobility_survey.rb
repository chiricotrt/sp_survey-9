class AddAttitudeToCarsColumnsToPrivateMobilitySurvey < ActiveRecord::Migration
  def change
    add_column :pre_survey_private_mobility_characteristics, :attitude_towards_cars_of_owner_1, :integer
    add_column :pre_survey_private_mobility_characteristics, :attitude_towards_cars_of_owner_2, :integer
    add_column :pre_survey_private_mobility_characteristics, :attitude_towards_cars_of_owner_3, :integer
    add_column :pre_survey_private_mobility_characteristics, :attitude_towards_cars_of_owner_4, :integer
    add_column :pre_survey_private_mobility_characteristics, :attitude_towards_cars_of_owner_5, :integer
    add_column :pre_survey_private_mobility_characteristics, :attitude_towards_cars_of_owner_6, :integer
    
    add_column :pre_survey_private_mobility_characteristics, :attitude_towards_cars_of_non_owner_1, :integer
    add_column :pre_survey_private_mobility_characteristics, :attitude_towards_cars_of_non_owner_2, :integer
    add_column :pre_survey_private_mobility_characteristics, :attitude_towards_cars_of_non_owner_3, :integer
    add_column :pre_survey_private_mobility_characteristics, :attitude_towards_cars_of_non_owner_4, :integer
    add_column :pre_survey_private_mobility_characteristics, :attitude_towards_cars_of_non_owner_5, :integer
    add_column :pre_survey_private_mobility_characteristics, :attitude_towards_cars_of_non_owner_6, :integer
  end
end
