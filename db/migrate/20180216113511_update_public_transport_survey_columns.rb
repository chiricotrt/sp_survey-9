class UpdatePublicTransportSurveyColumns < ActiveRecord::Migration
  def change
    remove_column :pre_survey_public_transport_characteristics, :pass_zones, :string
    remove_column :pre_survey_public_transport_characteristics, :other_pass_name, :string
    remove_column :pre_survey_public_transport_characteristics, :travel_passes, :string
    remove_column :pre_survey_public_transport_characteristics, :public_transport_passes, :string
    remove_column :pre_survey_public_transport_characteristics, :modes_most_used, :string

    add_column :pre_survey_public_transport_characteristics, :fare_reductions, :string
    add_column :pre_survey_public_transport_characteristics, :pass_holder, :integer
    add_column :pre_survey_public_transport_characteristics, :modes_with_valid_pass, :string
  end
end
