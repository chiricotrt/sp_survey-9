class AddAdditionalCarAttitudeQuestionsToPrivateSection < ActiveRecord::Migration
  def change
    add_column :pre_survey_private_mobility_characteristics, :attitude_towards_cars_of_owner_7, :integer
    add_column :pre_survey_private_mobility_characteristics, :attitude_towards_cars_of_owner_8, :integer
    add_column :pre_survey_private_mobility_characteristics, :attitude_towards_cars_of_owner_9, :integer
    add_column :pre_survey_private_mobility_characteristics, :attitude_towards_cars_of_owner_10, :integer
  end
end
