class AddSubLabelToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :sub_label, :string
  end
end
