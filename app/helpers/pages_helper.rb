module PagesHelper
  def link_to_android_app
    content_tag :strong do
      content_tag :span, class: 'class1' do
        link_to I18n.t('generic.here'), 'https://play.google.com/store/apps/details?id=edu.mit.smart.fmsurvey.android'
      end
    end
  end

  def link_to_iphone_app
    content_tag :strong do
      content_tag :span, class: 'class1' do
        link_to I18n.t('generic.here'),'https://itunes.apple.com/sg/app/fm-sensing/id604011160'
      end
    end
  end

  def link_to_smart
    content_tag(:span, link_to(I18n.t('generic.smart'),"http://smart.mit.edu/"), class: 'class1')
  end

  def link_to_privacy
    content_tag :span, class: 'class1' do
      link_to(I18n.t('generic.here'), "/docs/UCL_MaaS_Privacy_Final.pdf")
    end
  end

  def link_to_support
    content_tag :span, class: 'class1' do
      link_to( I18n.t('generic.support'), :controller => "pages", :action => :support)
    end
  end

  def link_to_faqs
    content_tag :span, class: 'class1' do
      link_to( I18n.t('menu.faqs'), :controller => "pages", :action => :faqs)
    end
  end
end
