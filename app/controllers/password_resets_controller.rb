class PasswordResetsController < ApplicationController
  impressionist

	before_filter :load_user_using_perishable_token, only: [:edit, :update]

	before_filter :require_no_user

	layout 'smart'

	def create
		@user = User.find_by_email(params[:email])

    if (@user.nil?)
      flash[:reset_pass] = I18n.t('forgotpass.no_user_found')
      flash[:class] = 'content_error'
      render :action => :new
    elsif (not @user.active)
      session[:attempt_email] = params[:email]
      redirect_to activations_new_path
    else
      @user.send_password_reset_instructions!
      flash[:notice] = I18n.t('forgotpass.instructions_sent_to_email')
      flash[:class] = 'content_feedback'
      redirect_to root_url
    end
  end

	def update
    if params[:user][:password] =='' or params[:user][:password_confirmation] == ''
      flash[:passwdupdated] = I18n.t('profile.passwd_cannot_be_empty')
      flash[:class] = "content_error"
    elsif params[:user][:password] != params[:user][:password_confirmation]
      flash[:passwdupdated] = I18n.t('profile.passwd_not_match')
      flash[:class] = "content_error"
    else
		  @user.password = params[:user][:password]
		  @user.password_confirmation = params[:user][:password_confirmation]
      @user.reset_single_access_token # Resets Single access token (API Auth)
		  if @user.save
        flash[:notice] = I18n.t('profile.passwd_successfully_changed')
        flash[:class] = "content_feedback"
        redirect_to pages_root_path
        return
		  else
			 render action: :edit
       return
      end
    end
    render action: :edit
	end

	private

	def load_user_using_perishable_token
		@user = User.find_using_perishable_token(params[:id], 1.day)
		unless @user
			flash[:notice] = I18n.t('forgotpass.not_found_perishable_token')
      flash[:class] = "content_error"
		  redirect_to root_url
	   end
  end
end
