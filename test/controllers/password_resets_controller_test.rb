require 'test_helper'

describe PasswordResetsController do
	fixtures(:users)

  describe "POST :create" do
  	before do
	  	@user = users(:one)
	  	post :create, {email: @user.email}
  	end

  	it "retrieves a user emails password reset instructions" do
  		assigns(:user).wont_be_nil
	  	ActionMailer::Base.deliveries.last.to.must_equal [@user.email]
	  	ActionMailer::Base.deliveries.last.subject.must_equal "Password Reset Instructions"
	  	must_redirect_to root_path
  	end
  end

  describe "PATCH :update" do
  	before do
  		@user = users(:one)
  		@old_pwd = @user.crypted_password
  		put :update, {id: @user.perishable_token, user: {password: "newpassword", password_confirmation: "newpassword"} }
  	end

  	it "updates the user password" do
  		assigns(:user).wont_be_nil
  		assert_not_equal(@user.reload.crypted_password, @old_pwd)
  	end
  end
end
